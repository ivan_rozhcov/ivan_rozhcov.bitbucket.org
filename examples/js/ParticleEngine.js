/**
 * @author Lee Stemkoski   http://www.adelphi.edu/~stemkoski/
 */

///////////////////////////////////////////////////////////////////////////////

/////////////
// SHADERS //
/////////////

// attribute: data that may be different for each particle (such as size and color);
//      can only be used in vertex shader
// varying: used to communicate data from vertex shader to fragment shader
// uniform: data that is the same for each particle (such as texture)

///////////////////////////////////////////////////////////////////////////////

/////////////////
// TWEEN CLASS //
/////////////////

SHOW_STATS = false;
SHOW_BUTTONS = false;
SHOW_ANIMATION = false;
SHOW_ANIMATION_MODEL = false;
ZOOMING = false;
ZOOMCOUNT = 0;
MAX_ZOOMCOUNT = 400;
SHOWED_PLANES = false;

SUBDIVISIONS = 90;
CAMERA_SUBDIVISIONS = MAX_ZOOMCOUNT;

END_SP_ANGLE = Math.PI/4;

SP_ACCELERATION = 8*END_SP_ANGLE/SUBDIVISIONS;

CROSS_SIZE = 11;
INITIAL_X = -75;
X = INITIAL_X - CROSS_SIZE;
Y = 180;
Z = -30;

Y_BACKGROUND_OFFSET = 10;

INITIAL_DELAY = 200;

GRID_TYPE = 'offset';

GRID_ROWS = 5;
GRID_COLLS = 13;
GRID = [];

CAMERA_OFFSET_Y = 220;
CAMERA_OFFSET_Z = 70;

PANE_Y_OFFSET = 100;

IsPlaneFaded = true;
BACKGROUND_PLANE = null;

CROSS_ON_THE_WAY = 0;

ALL_CROSSES_FIRED = false;

SHOW_BACKGROUND = false;

function textSprite(text, params) {
    var font = "Helvetica",
        size = 18,
        color = "#676767";

    font = "bold " + size + "px " + font;

    var canvas = document.createElement('canvas');
    var context = canvas.getContext('2d');
    context.font = font;

    // get size data (height depends only on font size)
    var metrics = context.measureText(text),
        textWidth = metrics.width;

    canvas.width = textWidth + 3;
    canvas.height = size + 3;

    context.font = font;
    context.fillStyle = color;
    context.fillText(text, 0, size + 3);

    // canvas contents will be used for a texture
    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;

    var mesh = new THREE.Mesh(
        new THREE.PlaneGeometry(canvas.width, canvas.height),
        new THREE.MeshBasicMaterial({
            map: texture
        }));

    return mesh;
}

fadeBackground  = function(){
    var part, _i, _len, _ref;

    _ref = this.engines[1].specialParticleArray;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        part = _ref[_i];
        if (part.particleMaterial.opacity !== 1) {
            part.particleMaterial.opacity += 0.01
        }
    }
    _ref = this.engines[0].specialParticleArray;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        part = _ref[_i];
        if (part.particleMaterial.opacity !== 1) {
            part.particleMaterial.opacity += 0.01;
        }
    }
    if(BACKGROUND_PLANE.material.opacity < 1){
        BACKGROUND_PLANE.material.opacity += 0.02;
    }
    else{
        BACKGROUND_PLANE.material.opacity = 1;
        IsPlaneFaded = true;
    }

}

showBackground = function(self){
    var geometry = new THREE.PlaneGeometry( idealWidth, idealHeight );
    var material = new THREE.MeshBasicMaterial( {color: 0x065B92, transparent: true, overdraw: true} );
    BACKGROUND_PLANE = new THREE.Mesh( geometry, material );

    BACKGROUND_PLANE.position.y = PANE_Y_OFFSET;
    BACKGROUND_PLANE.rotation.x = Math.PI / -2;
    scene.add(BACKGROUND_PLANE);
    BACKGROUND_PLANE.material.opacity = 0;

    IsPlaneFaded = false;

    $('#ThreeJS').append('<div style="position: fixed; bottom: 0px; z-index: 100;right: 0;left:  0; font-size: 5em; font-weight: bold; color: #ffffff;text-align: center; top: 20%;font: 8em/1 \'Open Sans\', sans-serif; font-weight: bold;">ТВОЯ ЭКОСИСТЕМА</div>');

    _ref = self.engines;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        var engine = _ref[_i];
        engine.emitterAlive = false;
        engine.destroy();
    }
}

timerChain = function(delaySecs, callbackArray) {
    var func;
    func = callbackArray.pop();
    if (func) {
        setTimeout((function() {
            func();
            timerChain(delaySecs, callbackArray);
        }), delaySecs);
    }
};

generateGrid = function() {
    var x, i, j, _i, _j, _k, _len;

    if (GRID_TYPE === 'offset') {
        var even = true;
        for (i = 0; i < GRID_ROWS; ++i) {
            X = INITIAL_X;
            Z += CROSS_SIZE;
            for (j = 0; j < GRID_COLLS; ++j) {
                X += CROSS_SIZE;
                if (even &&  X % 2 == 0)
                    GRID.push([X, Y, Z]);
                else
                    if (!even &&  X % 2 != 0)
                        GRID.push([X, Y, Z]);
            }
            even = ! even;
        }
    } else {
        for (i = 0; i < GRID_ROWS; ++i) {
            X = INITIAL_X;
            Z += CROSS_SIZE;
            for (j = 0; j < GRID_COLLS; ++j) {
                X += CROSS_SIZE;
                GRID.push([X, Y, Z]);
            }
        }
    }
    return GRID
};


function startSpecialParticles(self){

    generateGrid();

    var funArr = [];
    var i, _i;
    for (i = _i = 0; _i <= GRID.length - 2; i = ++_i) {
        if (Math.random() > 0.5) {
            funArr.push(function() {
                return self.engines[0].createSpecialParticle = true;
            });
        } else {
            funArr.push(function() {
                return self.engines[1].createSpecialParticle = true;
            });
        }

    }
    //последний крест (по времени) вылетел
    funArr.push(function() {
        ALL_CROSSES_FIRED = true;
        return self.engines[0].createSpecialParticle = true;
    });
    setTimeout(function() {
        ZOOMING = true;
    }, 500);

    setTimeout(function() {
        timerChain(120, funArr)
    }, INITIAL_DELAY)

}

function Tween(timeArray, valueArray)
{
    this.times  = timeArray || [];
    this.values = valueArray || [];
}

Tween.prototype.lerp = function(t)
{
    var i = 0;
    var n = this.times.length;
    while (i < n && t > this.times[i])
        i++;
    if (i == 0) return this.values[0];
    if (i == n)	return this.values[n-1];
    var p = (t - this.times[i-1]) / (this.times[i] - this.times[i-1]);
    if (this.values[0] instanceof THREE.Vector3)
        return this.values[i-1].clone().lerp( this.values[i], p );
    else // its a float
        return this.values[i-1] + p * (this.values[i] - this.values[i-1]);
}

///////////////////////////////////////////////////////////////////////////////

////////////////////
// PARTICLE CLASS //
////////////////////
function Particle()
{
    this.position     = new THREE.Vector3( -20,  2, 79 );
    this.velocity     = new THREE.Vector3(); // units per second
    this.acceleration = new THREE.Vector3();
    this.accelerationFunc = function(dt, t){
        this.acceleration.x = -this.K* this.position.x - this.L* this.velocity.x;
        this.acceleration.z =  - this.M*this.velocity.x;

        this.acceleration.add( this.randomVector3(new THREE.Vector3(0,0,10), new THREE.Vector3(3000, 0, 0)) );
        return this.acceleration
    };

    this.angle             = 0;
    this.angleVelocity     = 0; // degrees per second
    this.angleAcceleration = 0; // degrees per second, per second

    this.size = 16.0;

    this.color   = new THREE.Color();
    this.opacity = 1.0;

    this.age   = 0;
    this.alive = 0; // use float instead of boolean for shader purposes
    this.repulse = function ( target ) {

        var distance = this.position.distanceTo( target );
        if ( distance < 150 ) {

            var steer = new THREE.Vector3();

            steer.subVectors( this.position, target );
            steer.multiplyScalar( 0.5 / distance );

            this.acceleration.add( steer );

        }

    }


//    var floorTexture = new THREE.ImageUtils.loadTexture( 'textures/sprites/blue-cross-hi.png' );
//    this.particleMaterial = new THREE.MeshBasicMaterial( { map: floorTexture, transparent: true , opacity: 0.5, overdraw: true } );

//    this.particleMaterial = new THREE.MeshBasicMaterial( {color: 0x007FFF, transparent: true,  opacity: 0.5} );

    // geometry
    var circle = new THREE.Shape();


    var points, pos, _i, _len;
    points = [[0,4], [0,7], [4,7], [4,11], [7,11], [7,7], [11,7], [11,4], [7,4], [7,0],[4,0], [4,4], [0,4]];

    for (_i = 0, _len = points.length; _i < _len; _i++) {
      pos = points[_i];
      pos[0] -= 5.5;
      pos[1] -= 5.5;
      if (_i == 0) {
        circle.moveTo(pos[0], pos[1]);
      } else {
        circle.lineTo(pos[0], pos[1]);
      }
    }

    var geometry = circle.makeGeometry();
    this.particleMaterial = new THREE.MeshBasicMaterial({ color: 0x017EBE, transparent: true,  opacity: 0.5});
    var mesh = new THREE.Mesh(geometry, this.particleMaterial);

    // line
    this.particleMesh = mesh;

//    if ( Detector.webgl )
//        var floorGeometry = new THREE.PlaneBufferGeometry(10, 10, 1, 1);//    TODO так намного быстрее,но не работает на телефоне!
//    else
//        var floorGeometry = new THREE.PlaneGeometry(10, 10, 1, 1);

//    this.particleMesh = new THREE.Mesh(floorGeometry, this.particleMaterial);
    this.particleMesh.rotation.x = Math.PI / -2;
    this.particleMesh.position.x = this.position.x;
    this.particleMesh.position.y = this.position.y;
    this.particleMesh.position.z = this.position.z;
    scene.add( this.particleMesh );
}

Particle.prototype.destroy = function()
{
    scene.remove( this.particleMesh );
}

Particle.prototype.randomVector3 = function(base, spread)
{
    var rand3 = new THREE.Vector3( Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5 );
    return new THREE.Vector3().addVectors( base, new THREE.Vector3().multiplyVectors( spread, rand3 ) );
}

Particle.prototype.update = function(dt, t)
{
    this.position.add( this.velocity.clone().multiplyScalar(dt) );
    this.particleMesh.position.x = this.position.x;
    this.particleMesh.position.z = this.position.z;
    this.particleMesh.position.y = this.position.y;
    this.velocity.add( this.accelerationFunc(dt, t).clone().multiplyScalar(dt) );

    // convert from degrees to radians: 0.01745329251 = Math.PI/180
    this.angle         += this.angleVelocity     * 0.01745329251 * dt;
    this.angleVelocity += this.angleAcceleration * 0.01745329251 * dt;
    this.particleMesh.rotation.z = this.angle;

    this.age += dt;

    // if the tween for a given attribute is nonempty,
    //  then use it to update the attribute's value

//    if ( this.sizeTween.times.length > 0 )
//        this.size = this.sizeTween.lerp( this.age );
//
//    if ( this.colorTween.times.length > 0 )
//    {
//        var colorHSL = this.colorTween.lerp( this.age );
//        this.color = new THREE.Color().setHSL( colorHSL.x, colorHSL.y, colorHSL.z );
//    }

    if ( this.opacityTween.times.length > 0 )
        this.opacity = this.opacityTween.lerp( this.age );
}

//крестики летят в нужную нам точку по кривой безье
Particle.prototype.updateSpecial = function(dt, t)
{
    if (this.j < this.SUBDIVISIONS + 1) {
        var point = this.curve.getPoint(this.j / this.SUBDIVISIONS);
        this.particleMesh.position.x = point.x;
        this.particleMesh.position.z = point.z;
        this.particleMesh.position.y = point.y;
        this.j += 1;
        // convert from degrees to radians: 0.01745329251 = Math.PI/180
        this.angle         += this.angleAcceleration;
//        this.angleVelocity += 0.01745329251;
//        this.angleVelocity += this.angleAcceleration * 0.01745329251 * dt;
        this.particleMesh.rotation.z = this.angle;
//        CROSS_ON_THE_WAY = true;
    }
    else if (this.j == this.SUBDIVISIONS + 1) {
        this.particleMesh.position.x = this.destination.x;
        this.particleMesh.position.z = this.destination.z;
        this.particleMesh.position.y = this.destination.y;

        this.particleMesh.rotation.z = END_SP_ANGLE; //TODO почему не ровно?
//        TODO - если сделать так - то партиклы исчезают
        this.alive = false;
        CROSS_ON_THE_WAY -= 1;
    }
    else{
        this.alive = false;
    }

}

///////////////////////////////////////////////////////////////////////////////

///////////////////////////
// PARTICLE ENGINE CLASS //
///////////////////////////

var Type = Object.freeze({ "CUBE":1, "SPHERE":2 });

function ParticleEngine()
{
    /////////////////////////
    // PARTICLE PROPERTIES //
    /////////////////////////

    this.positionStyle = Type.CUBE;
    this.positionBase   = new THREE.Vector3( -20,  2, 79 );
    // cube shape data
    this.positionSpread = new THREE.Vector3();
    // sphere shape data
    this.positionRadius = 0; // distance from base at which particles start

    this.velocityStyle = Type.CUBE;
    // cube movement data
    this.velocityBase       = new THREE.Vector3();
    this.velocitySpread     = new THREE.Vector3();
    // sphere movement data
    //   direction vector calculated using initial position
    this.speedBase   = 0;
    this.speedSpread = 0;

    this.accelerationBase   = new THREE.Vector3();
    this.accelerationSpread = new THREE.Vector3();

    this.angleBase               = 0;
    this.angleSpread             = 0;
    this.angleVelocityBase       = 0;
    this.angleVelocitySpread     = 0;
    this.angleAccelerationBase   = 0;
    this.angleAccelerationSpread = 0;

    this.sizeBase   = 0.0;
    this.sizeSpread = 0.0;
    this.sizeTween  = new Tween();

    // store colors in HSL format in a THREE.Vector3 object
    // http://en.wikipedia.org/wiki/HSL_and_HSV
    this.colorBase   = new THREE.Vector3(0.0, 1.0, 0.5);
    this.colorSpread = new THREE.Vector3(0.0, 0.0, 0.0);
    this.colorTween  = new Tween();

    this.opacityBase   = 1.0;
    this.opacitySpread = 0.0;
    this.opacityTween  = new Tween();

    this.blendStyle = THREE.NormalBlending; // false;

    this.particleArray = [];
    this.specialParticleArray = [];
    this.particlesPerSecond = 100;
    this.particleDeathAge = 1.0;

    ////////////////////////
    // EMITTER PROPERTIES //
    ////////////////////////

    this.emitterAge      = 0.0;
    this.emitterAlive    = true;
    this.emitterDeathAge = 60; // time (seconds) at which to stop creating particles.

    // How many particles could be active at any time?
    this.particleCount = this.particlesPerSecond * Math.min( this.particleDeathAge, this.emitterDeathAge );

    //////////////
    // THREE.JS //
    //////////////

    this.particleGeometry = new THREE.Geometry();
    this.particleTexture  = null;

    var floorTexture = new THREE.ImageUtils.loadTexture( 'textures/sprites/gamepadopt.png' );
    this.particleMaterial = new THREE.MeshBasicMaterial( { map: floorTexture, transparent: true } );
    var floorGeometry = new THREE.PlaneGeometry(10, 10, 1, 1);
    this.particleMesh = new THREE.Mesh(floorGeometry, this.particleMaterial);
}

ParticleEngine.prototype.setValues = function( parameters ) {
    if (parameters === undefined) return;

    // clear any previous tweens that might exist
    this.sizeTween = new Tween();
    this.colorTween = new Tween();
    this.opacityTween = new Tween();

    for (var key in parameters)
        this[ key ] = parameters[ key ];

    // attach tweens to particles
    Particle.prototype.sizeTween = this.sizeTween;
    Particle.prototype.colorTween = this.colorTween;
    Particle.prototype.opacityTween = this.opacityTween;

    // calculate/set derived particle engine values
    this.particleArray = [];
    this.emitterAge = 0.0;
    this.emitterAlive = true;
    SCROLL = 1;
    this.particleCount = this.particlesPerSecond * Math.min(this.particleDeathAge, this.emitterDeathAge) * SCROLL;

    this.particleGeometry = new THREE.Geometry();
    this.particleTexture = null;

}

// helper functions for randomization
ParticleEngine.prototype.randomValue = function(base, spread)
{
    return base + spread * (Math.random() - 0.5);
}
ParticleEngine.prototype.randomVector3 = function(base, spread)
{
    var rand3 = new THREE.Vector3( Math.random() - 0.5, Math.random() - 0.5, Math.random() - 0.5 );
    return new THREE.Vector3().addVectors( base, new THREE.Vector3().multiplyVectors( spread, rand3 ) );
}


ParticleEngine.prototype.createParticle = function()
{
    var particle = new Particle();

    particle.position = this.randomVector3( this.positionBase, this.positionSpread );

    if ( this.velocityStyle == Type.CUBE )
    {
        particle.velocity     = this.randomVector3( this.velocityBase,     this.velocitySpread );
    }
    if ( this.velocityStyle == Type.SPHERE )
    {
        var direction = new THREE.Vector3().subVectors( particle.position, this.positionBase );
        var speed     = this.randomValue( this.speedBase, this.speedSpread );
        particle.velocity  = direction.normalize().multiplyScalar( speed );
    }

    particle.acceleration = this.randomVector3( this.accelerationBase, this.accelerationSpread );

    particle.angle             = this.randomValue( this.angleBase,             this.angleSpread );
    particle.angleVelocity     = this.randomValue( this.angleVelocityBase,     this.angleVelocitySpread );
    particle.angleAcceleration = this.randomValue( this.angleAccelerationBase, this.angleAccelerationSpread );

    particle.size = this.randomValue( this.sizeBase, this.sizeSpread );

    var color = this.randomVector3( this.colorBase, this.colorSpread );
    particle.color = new THREE.Color().setHSL( color.x, color.y, color.z );

    particle.opacity = this.randomValue( this.opacityBase, this.opacitySpread );

    particle.age   = 0;
    particle.alive = 0; // particles initialize as inactive

    particle.K = this.K;
    particle.L = this.L;
    particle.M = this.M;

    return particle;
}


//destination  THREE.Vector3()
ParticleEngine.prototype.createParticleSpecial = function(destination)
{
    var particle = new Particle();
    CROSS_ON_THE_WAY += 1;
    if (this.positionStyle == Type.CUBE)
        particle.position = this.randomVector3( this.positionBase, this.positionSpread );
    if (this.positionStyle == Type.SPHERE)
    {
        var z = 10 * Math.random() - 1;
        var t = 6.2832 * Math.random();
        var r = Math.sqrt( 1 - z*z );
        var vec3 = new THREE.Vector3( r * Math.cos(t), r * Math.sin(t), z );
        particle.position = new THREE.Vector3().addVectors( this.positionBase, vec3.multiplyScalar( this.positionRadius ) );
    }

    if ( this.velocityStyle == Type.CUBE )
    {
        particle.velocity     = this.randomVector3( this.velocityBase,     this.velocitySpread );
    }
    if ( this.velocityStyle == Type.SPHERE )
    {
        var direction = new THREE.Vector3().subVectors( particle.position, this.positionBase );
        var speed     = this.randomValue( this.speedBase, this.speedSpread );
        particle.velocity  = direction.normalize().multiplyScalar( speed );
    }

//	particle.acceleration = this.randomVector3( this.accelerationBase, this.accelerationSpread );

    particle.angle             = END_SP_ANGLE;
    particle.angleVelocity     = this.angleVelocityBase;
    particle.angleAcceleration = SP_ACCELERATION;

//	particle.angle             = this.randomValue( this.angleBase,             this.angleSpread );
//	particle.angleVelocity     = this.randomValue( this.angleVelocityBase,     this.angleVelocitySpread );
//	particle.angleAcceleration = this.randomValue( this.angleAccelerationBase, this.angleAccelerationSpread );

    particle.size = this.randomValue( this.sizeBase, this.sizeSpread );

    var color = this.randomVector3( this.colorBase, this.colorSpread );
//    particle.color = new THREE.Color().setHSL( color.x, color.y, color.z );

    particle.opacity = this.randomValue( this.opacityBase, this.opacitySpread );

    particle.age   = 0;
    particle.alive = true; // particles initialize as inactive

    //модуль скорости
//    particle.velocity;
    //начальная точка (x, y, z)
//    particle.position;
    //конечная точка x, y, z)
//    particle.destination = destination;
    particle.destination = destination;
    curve = new THREE.QuadraticBezierCurve3();
    curve.v0 = particle.position;
    curve.v1 = particle.velocity.multiplyScalar( 0.4 );
    curve.v2 = particle.destination;
    particle.curve = curve;
    particle.j = 0;
    particle.SUBDIVISIONS = SUBDIVISIONS;
    return particle;
}

ParticleEngine.prototype.initialize = function()
{
    // link particle data with geometry/material data
    for (var i = 0; i < this.particleCount; i++)
    {
        // remove duplicate code somehow, here and in update function below.
        this.particleArray[i] = this.createParticle();
    }

    this.particleMaterial.blending = this.blendStyle;
    if ( this.blendStyle != THREE.NormalBlending)
        this.particleMaterial.depthTest = false;
}

ParticleEngine.prototype.update = function(dt, t)
{
    var recycleIndices = [];

    // update particle data
    for (var i = 0; i < this.particleCount; i++)
    {
        if ( this.particleArray[i].alive )
        {
            this.particleArray[i].update(dt, t);

            // check if particle should expire
            // could also use: death by size<0 or alpha<0.
            if ( this.particleArray[i].age > this.particleDeathAge )
            {
                this.particleArray[i].alive = 0.0;
                recycleIndices.push(i);
            }
        }
    }

    // check if particle emitter is still running
    if ( !this.emitterAlive ) return;

    // if no particles have died yet, then there are still particles to activate
    if ( this.emitterAge < this.particleDeathAge )
    {
        // determine indices of particles to activate
        var startIndex = Math.round( this.particlesPerSecond * (this.emitterAge +  0) );
        var   endIndex = Math.round( this.particlesPerSecond * (this.emitterAge + dt) );
        if  ( endIndex > this.particleCount )
            endIndex = this.particleCount;

        for (var i = startIndex; i < endIndex; i++)
            this.particleArray[i].alive = 1.0;
    }

    // if any particles have died while the emitter is still running, we imediately recycle them
    for (var j = 0; j < recycleIndices.length; j++)
    {
        var i = recycleIndices[j];
        this.particleArray[i].destroy();
        this.particleArray[i] = this.createParticle();
        this.particleArray[i].alive = 1.0; // activate right away
        this.particleGeometry.vertices[i] = this.particleArray[i].position;
    }

    if (this.createSpecialParticle){
        //TODO сейчас не рандом!
        var el = GRID.pop(Math.floor(Math.random()*GRID.length));

        this.specialParticleArray.push(this.createParticleSpecial(new THREE.Vector3( el[0], el[1], el[2] )));
        this.createSpecialParticle = false;
    }

// update particle data
    for (var i = 0; i < this.specialParticleArray.length; i++)
    {
        if ( this.specialParticleArray[i].alive )
        {
            this.specialParticleArray[i].updateSpecial(dt, t);

            // check if particle should expire
            // could also use: death by size<0 or alpha<0.
        }
//        else
//        {
////            this.specialParticleArray.pop(i);
//            this.specialParticleArray.splice(i, 1);
//        }
    }


    // stop emitter?
    this.emitterAge += dt;
    if ( this.emitterAge > this.emitterDeathAge )  this.emitterAlive = false;


}

ParticleEngine.prototype.destroy = function()
{
    var el, _i, _len, _ref;

    _ref = this.particleArray;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        el = _ref[_i];
        el.destroy();
    }
}
///////////////////////////////////////////////////////////////////////////////

/**
* @author Lee Stemkoski   http://www.adelphi.edu/~stemkoski/
*/

Examples =
{

	
	// (1) build GUI for easy effects access.
	// (2) write ParticleEngineExamples.js
	
	// Not just any fountain -- a RAINBOW STAR FOUNTAIN of AWESOMENESS
	fountain :
	{
		positionBase     : new THREE.Vector3( -20,  2, 79 ),
		positionSpread   : new THREE.Vector3( 10, 0, 10 ),
		
		velocityStyle    : Type.CUBE,
		velocityBase     : new THREE.Vector3( 600,  0, 0 ),
		velocitySpread   : new THREE.Vector3( 300, 20, 10 ),

		accelerationBase : new THREE.Vector3( 0, 150, 0 ),
		accelerationSpread : new THREE.Vector3( 200, 0, 0 ),

//		particleTexture : THREE.ImageUtils.loadTexture( 'textures/sprites/plus.svg' ),

		angleBase               : 0,
		angleSpread             : 180,
		angleVelocityBase       : 0,
		angleVelocitySpread     : 360 * 4,
		
		sizeTween    : new Tween( [0, 1], [1, 20] ),
		opacityTween : new Tween( [2, 3], [1, 0] ),
		colorTween   : new Tween( [0.5, 2], [ new THREE.Vector3(0,1,0.5), new THREE.Vector3(0.8, 1, 0.5) ] ),

		particlesPerSecond : 8,
//		particlesPerSecond : 50,
//		particlesPerSecond : 0,
		particleDeathAge   : 3.0,
		emitterDeathAge    : 60,
        K: 20,
        L: 2,
        M: 1.5
	}
}